# flutter_supabase

Supabase auth demo project

based off supabase quickstart [here](https://supabase.io/docs/guides/with-flutter)

## Getting Started

create a supabase_config.dart file with the following in lib/

```
class SupabaseConfig {
  static const url = supabase url;
  static const anonKey = supabase anonKey;
}
```

where url and anonKey are from Settings > API in your Supabase project
