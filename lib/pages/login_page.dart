import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_supabase/components/auth_state.dart';
import 'package:flutter_supabase/utils/constants.dart';
import 'package:supabase/supabase.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends AuthState<LoginPage> {
  bool _isLoading = false;
  late final TextEditingController _emailController;
  late final TextEditingController _passwordController;

  Future<void> _signInWithMagicLink() async {
    setState(() {
      _isLoading = true;
    });
    final response = await supabase.auth.signIn(
        email: _emailController.text,
        options: AuthOptions(
            redirectTo: kIsWeb
                ? null
                : 'dev.luoma.flutter.supabase://login-callback/'));
    final error = response.error;
    if (error != null) {
      context.showErrorSnackBar(message: error.message);
    } else {
      context.showSnackBar(message: 'Check your email for login link!');
      _emailController.clear();
      _passwordController.clear();
    }

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _signInWithPassword() async {
    setState(() {
      _isLoading = true;
    });
    final response = await supabase.auth.signIn(
      email: _emailController.text,
      password: _passwordController.text,
    );
    final error = response.error;
    if (error != null) {
      context.showErrorSnackBar(message: error.message);
    } else if (response.data != null) {
      onAuthenticated(response.data!);
      _emailController.clear();
      _passwordController.clear();
    }

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _resetPassword() async {
    if (_emailController.text.isEmpty) {
      context.showErrorSnackBar(message: 'Enter your email');

      return;
    }

    final response = await supabase.auth.api.resetPasswordForEmail(
      _emailController.text,
      options: AuthOptions(
          redirectTo:
              kIsWeb ? null : 'dev.luoma.flutter.supabase://reset-callback/'),
    );

    final error = response.error;
    if (error != null) {
      context.showErrorSnackBar(message: error.message);
    } else {
      context.showSnackBar(message: 'Password reset link sent');
    }
  }

  @override
  void initState() {
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const padding = EdgeInsets.symmetric(vertical: 18, horizontal: 12);
    return Scaffold(
      appBar: AppBar(title: const Text('Sign In')),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: ListView(
              shrinkWrap: true,
              padding: padding,
              children: [
                const Text('Sign in via the magic link with your email, or '
                    'use your email and password'),
                const SizedBox(height: 18),
                TextFormField(
                  controller: _emailController,
                  decoration: const InputDecoration(labelText: 'Email'),
                ),
                const SizedBox(height: 18),
                ElevatedButton(
                  onPressed: _isLoading ? null : _signInWithMagicLink,
                  child: Text(_isLoading ? 'Loading' : 'Login with magic link'),
                ),
                TextFormField(
                  controller: _passwordController,
                  decoration: const InputDecoration(labelText: 'Password'),
                  obscureText: true,
                ),
                const SizedBox(height: 18),
                ElevatedButton(
                  onPressed: _isLoading ? null : _signInWithPassword,
                  child: Text(_isLoading ? 'Loading' : 'Login with password'),
                ),
                const SizedBox(height: 18),
              ],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 18, right: 18, top: 18, bottom: 27),
            child: SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: _isLoading ? null : _resetPassword,
                child: Text(_isLoading ? 'Loading' : 'Reset Password'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
